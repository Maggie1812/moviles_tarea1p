import 'package:flutter/material.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({
    Key key,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var currentColor = Colors.white;
  var _counter = 0;
  var _snackBarType;
  
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Click the FAB'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.thumb_up), 
            onPressed: (){
              _counter ++;
              _snackBarType = getSnackBar(_counter);
              currentColor == Colors.white ? currentColor = Colors.redAccent : currentColor = Colors.white;
              setState(() {
                _scaffoldKey.currentState.showSnackBar(_snackBarType);
              });
          },
          color: currentColor
          )
        ],
      ),
      body: Center(
        child: Container(
          child: Text('Clicks: ' +  _counter.toString()),
        )
      )
    );
  }

 SnackBar getSnackBar(int count){
   var _snackBar;
   count%2 == 0? _snackBar = getPlainSB('Even') : _snackBar = getDateTimeSB('Odd');
   return _snackBar;

 }

 SnackBar getPlainSB(String text){
   var snackBar = SnackBar(
     content: Text(text),
     duration: Duration(milliseconds: 150));
   return snackBar;
 }

 SnackBar getDateTimeSB(String text){
   var snackBar = SnackBar(
     content: Text(text),
     duration: Duration(milliseconds: 150), 
     action: SnackBarAction (
       label: 'Date',
       onPressed: (){
         showDateTimeDialog();
       }
     ));
   return snackBar;
 }

 void showDateTimeDialog(){
   var _dateTime = new DateTime.now();
   var _date = formatNumber(_dateTime.day) + '-' + formatNumber(_dateTime.month) + '-' + _dateTime.year.toString();
   var _time = formatNumber(_dateTime.hour) + ':' + formatNumber(_dateTime.minute);
   showDialog(
     context: context,
     builder: (BuildContext context){
       return AlertDialog(
         title: Text('Current Date Time'),
         content: Text( _time + ',' + _date),
         actions: <Widget>[
           new FlatButton(
             onPressed: () {
               Navigator.of(context).pop();
             }, 
           child: Icon(Icons.close))
         ]);
     }
   );
 }

 String formatNumber(int number){
   if (number < 10){
     return '0' + number.toString();
   }
   else return number.toString();
 }
}


